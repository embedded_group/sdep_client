package exception;

import packets.sError_tListPacket;

 
public class sError_tException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2783821364556515709L;
	private String errorName; //sError_tList OR errRetValue
	private sError_tListPacket err;

	public sError_tException(String s, sError_tListPacket err) {
		super();
		this.errorName = s;
		this.err = err;
	}
	
	public sError_tException(String s) {
		super();
		this.errorName = s;
		this.err = null;
	}

	public sError_tException() {
		super();
	}

	public sError_tListPacket getErr() {
		return err;
	}

	public void setErr(sError_tListPacket err) {
		this.err = err;
	}

	public String getErrorName() {
		return errorName;
	}

	public void setErrorName(String errorName) {
		this.errorName = errorName;
	}
	
	
}
