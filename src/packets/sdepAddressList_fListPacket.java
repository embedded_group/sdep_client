package packets;

import java.util.Vector;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import exception.sError_tException;

public class sdepAddressList_fListPacket implements MyJSON_Interface {
	
	private Vector<sdepAddressList_fPacket> addrs;

	public sdepAddressList_fListPacket(Vector<sdepAddressList_fPacket> addrs) {
		super();
		this.addrs = addrs;
	}

	public sdepAddressList_fListPacket() {
		super();
	}

	public Vector<sdepAddressList_fPacket> getAddrs() {
		return addrs;
	}

	public void setAddrs(Vector<sdepAddressList_fPacket> addrs) {
		this.addrs = addrs;
	}

	@Override
	public String convertObjectToJSON() throws JSONException {
	
		JSONArray arr = new JSONArray();
		
		for(int i=0; i<addrs.size(); i++) {
			JSONObject o = new JSONObject(addrs.get(i).convertObjectToJSON());
			arr.put(o);
		}
		
		return arr.toString();
	}

	@Override
	public void convertJSONToObject(String s) throws JSONException, sError_tException {
		
		Vector<sdepAddressList_fPacket> v = new Vector<sdepAddressList_fPacket>();
		
		JSONObject obj1 = new JSONObject(s);
		
		//case returnValue==sdepOK
		if (obj1.get(Constants.ret).toString().equals(Constants.OK)) {
			try {
				JSONArray o = obj1.getJSONArray(Constants.obj);
				
				for(int i=0; i<o.length(); i++) {
					JSONObject obj = o.getJSONObject(i);
					sdepAddressList_fPacket c = new sdepAddressList_fPacket();
					c.convertJSONToObject(obj.toString());
					v.add(c);
				}
				
				this.addrs = v;
			} catch (JSONException e) {
				e.printStackTrace();
				sError_tListPacket err = new sError_tListPacket();
				err.convertJSONToObject(s);	
				throw new sError_tException(Constants.errStr,err);
			}
		} else {
			System.out.println(obj1.toString());
			throw new sError_tException(obj1.get(Constants.ret).toString());
		}
	}

	@Override
	public String toString() {
		return "sdepAddressList_fListPacket [addrs=" + addrs + "]";
	}
	
	
	

}
