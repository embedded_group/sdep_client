package packets;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.Vector;

import org.json.JSONException;

import exception.sError_tException;

public interface IPacketBuilder {

	public abstract sCapability_tListPacket getProbeCapabilities(byte pAddr) throws
		JSONException, UnknownHostException, IOException, sError_tException;

	public abstract String sendProbeConfig(byte pAddr, sConfig_tListPacket cnList, int scNum)
		throws JSONException, UnknownHostException, IOException, sError_tException;

	public abstract sExchange_tPacket reqOneShotData(byte pAddr, sId_tPacket sensorId)
		throws JSONException, UnknownHostException, IOException, sError_tException;

	//returns the result of disconnect
	public abstract String sendDisconnect(byte pAddr) throws JSONException,
		UnknownHostException, IOException, sError_tException;

	public abstract sdepAddressList_fListPacket getProbeList() throws JSONException,
		UnknownHostException, IOException, sError_tException;
	
	public abstract Vector<PeriodicData> reqPeriodicElaboration(PeriodicConfigList perConfList, 
		int samplingInterval) throws JSONException, UnknownHostException,
			IOException, sError_tException;
	
	
	//public abstract sdepAddressList_fListPacket refreshProbeList() 
			//throws JSONException, UnknownHostException, IOException, sError_tException;

}