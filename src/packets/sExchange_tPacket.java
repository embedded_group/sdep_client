package packets;

import java.util.Arrays;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import exception.sError_tException;
import packets.Constants;

public class sExchange_tPacket implements MyJSON_Interface {
	
	/*Maximum number of samples*/
	public static final int NMAX = 255;	
	
	private sId_tPacket sensorId;
	private byte[] samplesPacket;
	
	
	public sExchange_tPacket() {
		super();
	}


	public sExchange_tPacket(sId_tPacket sensorId, byte[] samplesPacket) {
		super();
		this.sensorId = sensorId;
		this.samplesPacket = samplesPacket;
	}


	public sId_tPacket getSensorId() {
		return sensorId;
	}


	public void setSensorId(sId_tPacket sensorId) {
		this.sensorId = sensorId;
	}


	public byte[] getSamplesPacket() {
		return samplesPacket;
	}


	public void setSamplesPacket(byte[] samplesPacket) {
		this.samplesPacket = samplesPacket;
	}


	@Override
	public String convertObjectToJSON() throws JSONException {

		JSONObject obj = new JSONObject(); 
		
		sId_tPacket id = sensorId;
		JSONObject idJson = new JSONObject(id.convertObjectToJSON());
	
		obj.put(Constants.SENS_ID, idJson); 		
		obj.put(Constants.SAM_PKT, this.samplesPacket);

		return obj.toString(); 
	
	}


	@Override
	public void convertJSONToObject(String o) throws JSONException, sError_tException {
		
		sId_tPacket id = new sId_tPacket();	
		
		JSONObject obj = new JSONObject(o);

		//case returnValue==sdepOK
		//if (objj.get(Constants.ret).toString().equals(Constants.OK)) {
			try {
				
				//JSONObject obj = objj.getJSONObject(Constants.obj);
				
				JSONObject json_id = obj.getJSONObject(Constants.SENS_ID);
				
				id.convertJSONToObject(json_id.toString());
				
				this.sensorId = id; 
				
				JSONArray arr = obj.getJSONArray(Constants.SAM_PKT);
				byte[] samples = new byte[arr.length()]; 
				for(int i=0 ;i< arr.length();i++)
					samples[i] = (byte)arr.getInt(i); 
		
				this.samplesPacket = samples; 
			} catch (JSONException e) {
				e.printStackTrace();
				sError_tListPacket err = new sError_tListPacket();
				err.convertJSONToObject(o);	
				throw new sError_tException("BAD_FORMAT_sExchange_tPacket");
			}
		//} else {
			//throw new sError_tException(objj.get(Constants.ret).toString());
		//}
	}


	//DEBUG
	@Override
	public String toString() {
		return "sExchange_tPacket [sensorId=" + sensorId + ", samplesPacket="
				+ Arrays.toString(samplesPacket) + "]";
	}
	
	
	
	
}
