package packets;

import org.json.JSONException;
import org.json.JSONObject;

import exception.sError_tException;

public class sId_tPacket implements MyJSON_Interface {
	
	private byte sensorType;
	private byte sensorNum;
	
	
	public sId_tPacket(byte sensorType, byte sensorNum) {
		super();
		this.sensorType = sensorType;
		this.sensorNum = sensorNum;
	}


	public sId_tPacket() {
		// TODO Auto-generated constructor stub
	}


	public byte getSensorType() {
		return sensorType;
	}


	public void setSensorType(byte sensorType) {
		this.sensorType = sensorType;
	}


	public byte getSensorNum() {
		return sensorNum;
	}


	public void setSensorNum(byte sensorNum) {
		this.sensorNum = sensorNum;
	}


	@Override
	public String convertObjectToJSON() throws JSONException {

		JSONObject o = new JSONObject(); 
		
		o.put(Constants.SENS_TYPE, this.sensorType); 
		o.put(Constants.SENS_NUM, this.sensorNum);  		
		
		return o.toString(); 
		
	}


	@Override
	public void convertJSONToObject(String s) throws JSONException, sError_tException {
			
		JSONObject o = new JSONObject(s);

		//case returnValue==sdepOK
		//if (obj.get(Constants.ret).toString().equals(Constants.OK)) {
			try {
				
				//JSONObject o = obj.getJSONObject(Constants.obj);
				this.sensorType = (byte)o.getInt(Constants.SENS_TYPE);
				this.sensorNum = (byte)o.getInt(Constants.SENS_NUM); 
			} catch (JSONException e) {
				e.printStackTrace();
				sError_tListPacket err = new sError_tListPacket();
				err.convertJSONToObject(s);	
				throw new sError_tException(Constants.errStr,err);
			}
		//} else {
			//throw new sError_tException(obj.get(Constants.ret).toString());
		//}
	}


	@Override
	public String toString() {
		return "sId_tPacket [sensorType=" + sensorType + ", sensorNum="
				+ sensorNum + "]";
	};
	
	
	
	
	
	
	

}
