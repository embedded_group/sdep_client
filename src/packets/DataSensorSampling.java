package packets;


import org.json.JSONException;
import org.json.JSONObject;

import exception.sError_tException;

public class DataSensorSampling implements MyJSON_Interface {

	private int periodSampling;
	private sId_tPacket sensorSID;
	private int sampleLen;
	private int sampleMode; 
	
	
	public DataSensorSampling(int periodSampling, sId_tPacket sids,
			  int sampleLen, int samplingMode) {
		super();
		this.periodSampling = periodSampling;
		this.sensorSID = sids;
		this.sampleLen= sampleLen;
		this.sampleMode = samplingMode; 
	}
	
	
	@Override
	public String convertObjectToJSON() throws JSONException {
		JSONObject obj = new JSONObject(); 
		obj.put(Constants.JSON_KEY_SAMPLING_TIME, periodSampling); 
		obj.put(Constants.JSON_KEY_SAMPLE_LEN, sampleLen); 
		obj.put(Constants.SENS_ID, sensorSID);
		obj.put(Constants.JSON_KEY_SAMPLE_MODE, sampleMode);
		
		
		return obj.toString();
		
		
	}
	@Override
	public void convertJSONToObject(String o) throws JSONException,
			sError_tException {
		// TODO Auto-generated method stub
		
	} 
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
