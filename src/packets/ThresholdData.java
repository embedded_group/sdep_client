package packets;

import java.util.Arrays;
import java.util.Vector;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import exception.sError_tException;

public class ThresholdData implements MyJSON_Interface {
	
	private byte pAddr;
	private sId_tPacket sensorId;
	private byte intType;
	private byte[] samplesPacket;
	
	
	public ThresholdData() {
		super();
	}
	
	public ThresholdData(byte pAddr, sId_tPacket sensorId, byte intType,
			byte[] samplesPacket) {
		
		super();
		this.pAddr = pAddr;
		this.sensorId = sensorId;
		this.intType = intType;
		this.samplesPacket = samplesPacket;
		
	}
	
	
	public byte getpAddr() {
		return pAddr;
	}
	public void setpAddr(byte pAddr) {
		this.pAddr = pAddr;
	}
	public sId_tPacket getSensorId() {
		return sensorId;
	}
	public void setSensorId(sId_tPacket sensorId) {
		this.sensorId = sensorId;
	}
	public byte getIntType() {
		return intType;
	}
	public void setIntType(byte intType) {
		this.intType = intType;
	}
	public byte[] getSamplesPacket() {
		return samplesPacket;
	}
	public void setSamplesPacket(byte[] samplesPacket) {
		this.samplesPacket = samplesPacket;
	}

	
	@Override
	public String convertObjectToJSON() throws JSONException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void convertJSONToObject(String s) throws JSONException,
			sError_tException {
		
		JSONObject objj = new JSONObject(s);
		
		//case returnValue==sdepOK
		if (objj.get(Constants.ret).toString().equals(Constants.OK)) {
			try {
							
				JSONObject o = objj.getJSONObject(Constants.obj);
				
				this.pAddr = (byte)o.getInt(Constants.paddr);
				
				JSONObject json_id = o.getJSONObject(Constants.SENS_ID);
				sId_tPacket id = new sId_tPacket();		
				id.convertJSONToObject(json_id.toString());
				this.sensorId = id; 
				
				this.intType = (byte)o.getInt(Constants.TYPE);
			
				JSONArray arr = o.getJSONArray(Constants.SAM_PKT);
				byte[] samples = new byte[arr.length()]; 
				for(int i=0 ;i< arr.length();i++)
					samples[i] = (byte)arr.getInt(i); 
		
				this.samplesPacket = samples;

			} catch (JSONException e) {
				e.printStackTrace();
				sError_tListPacket err = new sError_tListPacket();
				err.convertJSONToObject(s);	
				throw new sError_tException(Constants.errStr,err);
			}
		} else {
			throw new sError_tException(objj.get(Constants.ret).toString());
		}
		
	}

	@Override
	public String toString() {
		return "ThresholdData [pAddr=" + pAddr + ", sensorId=" + sensorId
				+ ", intType=" + intType + ", samplesPacket="
				+ Arrays.toString(samplesPacket) + "]";
	}

	

}
