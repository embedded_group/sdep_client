package packets;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Observable;

import org.json.JSONException;

import exception.sError_tException;

public class ThresholdThread extends Observable implements Runnable {

	private int port;
	private ThresholdData td;
	private ServerSocket thresholdSocket;
	private boolean loopVariable = true; 
	
	
	public ThresholdThread() {
		super();
	}


	public ThresholdThread(int port, ThresholdData td,
			ServerSocket thresholdSocket) {
		super();
		this.port = port;
		this.td = td;
		this.thresholdSocket = thresholdSocket;
	}
	
	public ThresholdThread(int port) {
		super();
		this.port = port;
		this.td = new ThresholdData();
	}



	public int getPort() {
		return port;
	}


	public void setPort(int port) {
		this.port = port;
	}


	public ThresholdData getTd() {
		return td;
	}


	public void setTd(ThresholdData td) {
		this.td = td;
	}


	public boolean isLoopVariable() {
		return loopVariable;
	}


	public void setLoopVariable(boolean loopVariable) {
		this.loopVariable = loopVariable;
	}


	@Override
	public void run() {

		try {
			
		thresholdSocket = new ServerSocket(port);
		
				while(isLoopVariable()) {
								
					System.out.println("[THREAD CLIENT] Listening...\n");
					Socket connectionSocket = thresholdSocket.accept();
					System.out.println("[THREAD CLIENT] Uscito dalla listen\n");
				
						try {
				
							BufferedReader inFromServer = new BufferedReader(new InputStreamReader(
								connectionSocket.getInputStream()));  
	
							String serverIn = inFromServer.readLine();
				
							//DEBUG
							System.out.println("[THREAD CLIENT] Received: "+serverIn+"\n");
							//END DEBUG
						
							td.convertJSONToObject(serverIn);
							System.out.println("[THREAD CLIENT] Fuori da convertJSONObject\n");
		
							setChanged();
							notifyObservers(td);
							
				
							} catch (IOException io_e) {
								io_e.printStackTrace();
							} catch (JSONException json_e) {
								json_e.printStackTrace();
							} catch (sError_tException err_e) {
								err_e.printStackTrace();
							} finally {
								
								System.out.println("[THREAD CLIENT] Sto per chiudere connectionSocket\n");
								connectionSocket.close();
								System.out.println("[THREAD CLIENT] Chiusa connectionSocket\n");
					
							}

				} //end while
		
			} catch (IOException e) {
				e.printStackTrace();
			}
	
		}
		
}
