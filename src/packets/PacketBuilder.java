package packets;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.Vector;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import exception.sError_tException;

public class PacketBuilder implements IPacketBuilder {
	
	
	/*************************************************
	 * -------JSONObject format sent by Client--------
	 *Json_obj {
	 *	"methodName" 	: nome_metodo,
	 *	"pAddress"	 	: probeAddress,
	 *	"variableField"	: a_parameter,
	 *  "object"		: encapsulated_object
	 * 	}
	 * -----------------------------------------------
	 *************************************************
	 *************************************************
	 *************************************************
	 * ----JSONObject format received from Server-----
	 *Json_obj {
	 *	"return"		: "return_value",
	 *  "object"		: encapsulated_object
	 * 	}
	 * -----------------------------------------------
	 *************************************************
	 */
	private JSONObject obj;
	private String address;
	private int port;
	
	
//	public PacketBuilder() {
//		this.obj = new JSONObject();
//		this.address = Constants.IP; 
//		this.port = Constants.PORT; 
//	}
	
	public PacketBuilder(String address, int port) {
		this.obj = new JSONObject();
		this.address = address; 
		this.port = port; 
	}
	
 


	@Override
	public sCapability_tListPacket getProbeCapabilities(byte pAddr) throws
		JSONException, UnknownHostException, IOException, sError_tException {
		
		this.obj.put(Constants.met, Constants.S_CAPABILITY);
		this.obj.put(Constants.paddr, pAddr);
		this.obj.put(Constants.var, Constants.NULL);
		this.obj.put(Constants.obj, Constants.NULL);
		
		PacketSender sender = new PacketSender(address, port);
		String s = sender.exchangeData(this.obj);
		
		//Server should return the specified formatted object
		sCapability_tListPacket cap = new sCapability_tListPacket();
		cap.convertJSONToObject(s);
		
		return cap;
	}
	
	

	@Override
	public String sendProbeConfig(byte pAddr, sConfig_tListPacket cnList, int scNum)
		throws JSONException, UnknownHostException, IOException, sError_tException {
		

		this.obj.put(Constants.met, Constants.S_CONFIG);
		this.obj.put(Constants.paddr, pAddr);
		this.obj.put(Constants.var, scNum);
		this.obj.put(Constants.obj, cnList.convertObjectToJSON());
		
		//System.out.println("cnList:"+cnList.convertObjectToJSON());
		PacketSender sender = new PacketSender(address, port);
		String s = sender.exchangeData(this.obj);
		
		//Server should return the specified formatted object
		JSONObject objj = new JSONObject(s);
		return objj.get(Constants.ret).toString();

	}
	
	
	

	@Override
	public sExchange_tPacket reqOneShotData(byte pAddr, sId_tPacket sensorId) 
		throws JSONException, UnknownHostException, IOException, sError_tException {
		
		this.obj.put(Constants.met, Constants.JSON_KEY_ONESHOT_REQ);
		this.obj.put(Constants.paddr, pAddr);
		this.obj.put(Constants.var, sensorId.convertObjectToJSON());
		this.obj.put(Constants.obj, Constants.NULL);
		
		PacketSender sender = new PacketSender(address, port);
		String s = sender.exchangeData(this.obj);
		
		//Server should return the specified formatted object
		sExchange_tPacket data = new sExchange_tPacket();		
		data.convertJSONToObject(s);
		
		return data;
	}
	
	
	//returns the result of disconnect
	@Override
	public String sendDisconnect(byte pAddr) throws JSONException,
		UnknownHostException, IOException, sError_tException {
		
		this.obj.put(Constants.met, Constants.S_DISCONNECT);
		this.obj.put(Constants.paddr, pAddr);
		this.obj.put(Constants.var, Constants.NULL);
		this.obj.put(Constants.obj, Constants.NULL);
		
		PacketSender sender = new PacketSender(address, port);
		String s = sender.exchangeData(this.obj);
	
		//Server should return the specified formatted object
		JSONObject objj = new JSONObject(s);
		return objj.get(Constants.ret).toString();

	}
	
	

	@Override
	public sdepAddressList_fListPacket getProbeList() throws JSONException,
		UnknownHostException, IOException, sError_tException {
	
		this.obj.put(Constants.met, Constants.G_ATT_LIST);
		this.obj.put(Constants.paddr, Constants.NULL);
		this.obj.put(Constants.var, Constants.NULL);
		this.obj.put(Constants.obj, Constants.NULL);
		
		//calls getAttachedList()
		PacketSender sender = new PacketSender(address, port);
		String s = sender.exchangeData(this.obj);
		
		//System.out.println("s:"+s);
		
		//Server should return the specified formatted object
		sdepAddressList_fListPacket res = new sdepAddressList_fListPacket();
		
		/*JSONObject obj1 = new JSONObject(s);
		System.out.println(obj1.get(Constants.ret).toString());
		if (!obj1.get(Constants.ret).toString().equals(Constants.OK)) {
			throw new sError_tException(obj1.get(Constants.ret).toString());

		}		
		try {*/
		
			res.convertJSONToObject(s);
			
		/*}
		catch (JSONException e) {
			System.out.println("Sono qui");
			e.printStackTrace();
			System.out.println("sss:"+s);
			sError_tListPacket err = new sError_tListPacket();
			err.convertJSONToObject(s);	
			throw new sError_tException(Constants.errStr,err);
		}*/
	
		return res;
		
	}


	@Override
	public Vector<PeriodicData> reqPeriodicElaboration(PeriodicConfigList perConfList,
		int samplingInterval) throws JSONException, UnknownHostException,
			IOException, sError_tException {

		this.obj.put(Constants.met, Constants.JSON_KEY_PERIODIC_REQ);
		this.obj.put(Constants.paddr, Constants.NULL);
		this.obj.put(Constants.var, samplingInterval);
		this.obj.put("numProbes", perConfList.getPerConfList().size());
		this.obj.put(Constants.obj, perConfList.convertObjectToJSON());
		
		PacketSender sender = new PacketSender(address, port);
		String s = sender.exchangeData(this.obj);
		
		JSONObject o = new JSONObject(s);
		System.out.println(o);
		
		Vector<PeriodicData> perData = 
			new Vector<PeriodicData>(perConfList.getPerConfList().size());

		JSONArray arr1 = o.getJSONArray(Constants.data);
		JSONArray arr2 = o.getJSONArray("pAddrList");

		for(int i=0; i<perConfList.getPerConfList().size(); i++) {
			
			sExchange_tListPacket data = new sExchange_tListPacket();
			data.convertJSONToObject(arr1.get(i).toString());
			
			PeriodicData p = new PeriodicData((byte)arr2.getInt(i),data);
			
			perData.add(p);
		
		}
		
		return perData;

	}

	/*public sdepAddressList_fListPacket refreshProbeList() 
			throws JSONException, UnknownHostException, IOException, sError_tException {
		
		this.obj.put(Constants.met, Constants.C_ALIVE);
		this.obj.put(Constants.paddr, Constants.NULL);
		this.obj.put(Constants.var, Constants.NULL);
		this.obj.put(Constants.obj, Constants.NULL);
		
		//calls checkAlive() in loop
		PacketSender sender = new PacketSender();
		String s = sender.exchangeData(this.obj);
		
		//Server should return the specified formatted object
		sdepAddressList_fListPacket res = new sdepAddressList_fListPacket();
		res.convertJSONToObject(s);
		
		return res;
	}*/

}
