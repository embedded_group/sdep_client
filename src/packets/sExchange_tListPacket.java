package packets;

import java.util.Vector;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import exception.sError_tException;

public class sExchange_tListPacket implements MyJSON_Interface {
	
	private Vector<sExchange_tPacket> data;
	

	public sExchange_tListPacket() {
		super();
	}


	public sExchange_tListPacket(Vector<sExchange_tPacket> data) {
		super();
		this.data = data;
	}


	public Vector<sExchange_tPacket> getData() {
		return data;
	}


	public void setData(Vector<sExchange_tPacket> data) {
		this.data = data;
	}


	
	//not used
	@Override
	public String convertObjectToJSON() throws JSONException {

		JSONArray arr = new JSONArray();
		
		for(int i=0; i<data.size(); i++) {
			JSONObject o = new JSONObject(data.get(i).convertObjectToJSON());
			arr.put(o);
		}
		
		return arr.toString();
		
	}

	

	@Override
	public void convertJSONToObject(String s) throws JSONException,
			sError_tException {
		
		Vector<sExchange_tPacket> v = new Vector<sExchange_tPacket>();
		JSONObject obj1 = new JSONObject(s);
		
		//case returnValue==sdepOK
		if (obj1.get(Constants.ret).toString().equals(Constants.OK)) {
			try {
				JSONArray o = obj1.getJSONArray(Constants.obj);
				
				for(int i=0; i<o.length(); i++) {
					JSONObject obj = o.getJSONObject(i);
					sExchange_tPacket c = new sExchange_tPacket();
					c.convertJSONToObject(obj.toString());
					v.add(c);
				}
				
				this.data = v;
			} catch (JSONException e) {
				e.printStackTrace();
				sError_tListPacket err = new sError_tListPacket();
				err.convertJSONToObject(s);	
				throw new sError_tException(Constants.errStr,err);
			}
		} else {
			throw new sError_tException(obj1.get(Constants.ret).toString());
		}
		
	}


	@Override
	public String toString() {
		return "sExchange_tListPacket [data=" + data + "]";
	}
	
	

	
}
