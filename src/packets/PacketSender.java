package packets;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.*;
import java.net.*;

public class PacketSender {
	
    private Socket clientSocket;
	private String address;
	private int port;
    

	public PacketSender(String address, int port) {
	
		this.address= address; 
		this.port = port;
	}
	
	public String exchangeData(JSONObject o) throws JSONException, UnknownHostException, IOException {
		
		this.clientSocket = new Socket(address, port);

		String out = o.toString();
        DataOutputStream outToServer = new DataOutputStream(this.clientSocket.getOutputStream());        
        BufferedReader inFromServer = new BufferedReader(new InputStreamReader(this.clientSocket.getInputStream()));
        
        System.out.println("to send:"+out);
        //NB: la stringa da inviare deve terminare necessariamente con una endline
        outToServer.writeBytes(out+'\n');
        
        String in = inFromServer.readLine();
        
		clientSocket.close(); 
        
        return in;
	
	}
	
	

}
