package packets;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.Vector;

import org.json.JSONException;
import exception.sError_tException;
import packets.Constants;

public class Test {

	public static void main(String[] args) throws UnknownHostException,
		JSONException, IOException, sError_tException {
		
		ThresholdThread thread = new ThresholdThread(7099); 
		Thread t = new Thread(thread);
		t.start();
		thread.setLoopVariable(true);
		
		System.out.println("server in attesa su porto 7099");
		
		PacketBuilder pktBdr = new PacketBuilder(Constants.IP, Constants.PORT);		
		
		testAttached(pktBdr);
		
		testCapability(pktBdr,(byte)0);
		
		
		sId_tPacket id1 = new sId_tPacket((byte)0,(byte)0);
		sId_tPacket id2 = new sId_tPacket((byte)1,(byte)0);
		sId_tPacket id3 = new sId_tPacket((byte)2,(byte)0);
		sId_tPacket id4 = new sId_tPacket((byte)3,(byte)0);
		
		testConfig(pktBdr,id1,id2,id3,id4,(byte)0);
		
		
		
		/*
		testExchangeData(pktBdr, id1,(byte)0);
		

		testDisconnect(pktBdr,(byte)0);
		

		/*probes = pktBdr.refreshProbeList();
		System.out.println("Res from "+Constants.C_ALIVE+": "+probes);*/

	
		
		
		
		
		/*
		byte x1=2, x2=3, x3=4;
		sCapability_tPacket c1 = new sCapability_tPacket(x1,x2,x3);
		
		byte x11=21, x21=31, x31=41;
		sCapability_tPacket c2 = new sCapability_tPacket(x11,x21,x31);
		
		byte x12=22, x22=32, x32=42;
		sCapability_tPacket c3 = new sCapability_tPacket(x12,x22,x32);
		
		Vector<sCapability_tPacket> v = new Vector<sCapability_tPacket>();
		v.add(c1);
		v.add(c2);
		v.add(c3);
		
		sCapability_tListPacket c = new sCapability_tListPacket(v);
		
		String s = new String(c.convertObjectToJSON());
		
		System.out.println(s);		
		
		sCapability_tListPacket cc = new sCapability_tListPacket();
		cc.convertJSONToObject(s);
		
		System.out.println("\n");
		System.out.println(cc);		
		
		JSONObject o = new JSONObject();
		o.put("sesso", 23);
		o.put("cane", 25);
		System.out.println(o);
		
		o.put("sesso", 5);
		System.out.println(o);
		*/
		
//		sdepAddressList_fPacket a = new sdepAddressList_fPacket((byte)12,4);
//		sdepAddressList_fPacket b = new sdepAddressList_fPacket((byte)3,13);
//		Vector<sdepAddressList_fPacket> v = new Vector<sdepAddressList_fPacket>();
//		v.add(a);
//		v.add(b);
//		sdepAddressList_fListPacket vav = new sdepAddressList_fListPacket(v);
//
//		String s = new String(vav.convertObjectToJSON());
//		vav.convertJSONToObject(s);
//		
//		System.out.println(s);
//		
	}

	
	public static void testDisconnect(PacketBuilder pktBdr, byte pAdd)
			throws JSONException, UnknownHostException, IOException,
			sError_tException 
	{
		String res2 = (String) pktBdr.sendDisconnect(pAdd);
		System.out.println("Res from "+Constants.S_DISCONNECT+": "+res2);
	}

	public static void testExchangeData(PacketBuilder pktBdr, sId_tPacket id1,
		byte pAdd) throws JSONException, UnknownHostException, IOException,
			sError_tException 
	{
		sExchange_tPacket data = new sExchange_tPacket();
		data = pktBdr.reqOneShotData(pAdd, id1);
		System.out.println("Res from "+Constants.S_SAMPLE_REQ+": "+data.toString());
	}

	public static void testConfig(PacketBuilder pktBdr, sId_tPacket id1, sId_tPacket id2, sId_tPacket id3,
			sId_tPacket id4, byte pAdd)throws JSONException, UnknownHostException,
	IOException, sError_tException 
	{

		sConfig_tPacket c1 = new sConfig_tPacket(id1,(byte)0,(byte)0);
		sConfig_tPacket c2 = new sConfig_tPacket(id2,(byte)176,(byte)1);
		sConfig_tPacket c3 = new sConfig_tPacket(id1,(byte)0,(byte)0);
		sConfig_tPacket c4 = new sConfig_tPacket(id1,(byte)0,(byte)0);
		Vector<sConfig_tPacket> v = new Vector<sConfig_tPacket>();
		v.add(c1);
		v.add(c2);
		v.add(c3);
		v.add(c4);

		sConfig_tListPacket confList = new sConfig_tListPacket(v);
		String res = pktBdr.sendProbeConfig(pAdd, confList, 4);
		System.out.println("Res from "+Constants.S_CONFIG+": "+res);
	}

	public static void testCapability(PacketBuilder pktBdr, byte pAdd)
			throws JSONException, UnknownHostException, IOException,
			sError_tException {
		sCapability_tListPacket capList = new sCapability_tListPacket();
		capList = pktBdr.getProbeCapabilities(pAdd);
		System.out.println("Res from "+Constants.S_CAPABILITY+": "+capList.toString());
	}

	public static void testAttached(PacketBuilder pktBdr) throws JSONException,
			UnknownHostException, IOException, sError_tException {
		sdepAddressList_fListPacket probes = new sdepAddressList_fListPacket();
		probes = pktBdr.getProbeList();
		System.out.println("Res from "+Constants.G_ATT_LIST+": "+probes);
	}

}
