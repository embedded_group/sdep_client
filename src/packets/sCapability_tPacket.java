package packets;

import org.json.JSONException;
import org.json.JSONObject;

import exception.sError_tException;
import packets.Constants;

public class sCapability_tPacket implements MyJSON_Interface {
	
	
	private byte sensorType;
	private byte sensorQuantity;
	private byte sensorCap;

	
	public sCapability_tPacket() {
	}


	public sCapability_tPacket(byte st, byte sq, byte sl) {
		super();
		
		sensorType = st;
		sensorQuantity = sq;
		sensorCap = sl;
		
	}

	public byte getSensorType() {
		return sensorType;
	}

	public void setSensorType(byte sensorType) {
		this.sensorType = sensorType;
	}

	public byte getSensorQuantity() {
		return sensorQuantity;
	}

	public void setSensorQuantity(byte sensorQuantity) {
		this.sensorQuantity = sensorQuantity;
	}

	public byte getSensorCap() {
		return sensorCap;
	}

	public void setSensorCap(byte sensorCap) {
		this.sensorCap = sensorCap;
	}

	@Override
	public String convertObjectToJSON() throws JSONException {
		
		JSONObject o = new JSONObject(); 
	
		o.put(Constants.SENS_TYPE, this.sensorType); 
		o.put(Constants.CAP_QUAN, this.sensorQuantity); 
		o.put(Constants.CAP_LEN,this.sensorCap); 		
		
		return o.toString(); 
		
	}

	@Override
	public void convertJSONToObject(String s) throws JSONException, sError_tException {
					
		JSONObject o = new JSONObject(s);
		
		//case returnValue==sdepOK
		//if (obj.get(Constants.ret).toString().equals(Constants.OK)) {
			try {
				
				//JSONObject o = obj.getJSONObject(Constants.obj);
				this.sensorType = (byte)o.getInt(Constants.SENS_TYPE);
				this.sensorQuantity = (byte)o.getInt(Constants.CAP_QUAN); 
				this.sensorCap = (byte)o.getInt(Constants.CAP_LEN);
			} catch (JSONException e) {
				e.printStackTrace();
				sError_tListPacket err = new sError_tListPacket();
				err.convertJSONToObject(s);	
				throw new sError_tException(Constants.errStr,err);
			}
		//} else {
			//throw new sError_tException(obj.get(Constants.ret).toString());
		//}
	
	}


	@Override
	public String toString() {
		return "sCapability_tPacket [sensorType=" + sensorType
				+ ", sensorQuantity=" + sensorQuantity + ", sampleCap="
				+ sensorCap + "]";
	}

	
}
