package packets;


import org.json.JSONException;

import exception.sError_tException;

public interface MyJSON_Interface{

	public abstract String convertObjectToJSON() throws JSONException;
	
	public abstract void convertJSONToObject(String o) throws
		JSONException, sError_tException; 

	/*public static String fillJSON(List<? extends MyJSON_Interface> list) throws JSONException
	{
		
		JSONArray arr = new JSONArray(); 
		for(MyJSON_Interface s : list)
		{
			arr.put(s.convertObjectToJSON()); 
		}
		
		return arr.toString(); 
	}
	*/
}
