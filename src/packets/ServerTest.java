package packets;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Vector;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class ServerTest {

	public static void main(String[] args) throws IOException, JSONException {
		
		String clientIn; 
		ServerSocket welcomeSocket = new ServerSocket(Constants.PORT); 
		
		while(true) {
			
			System.out.println("[SERVER] Waiting...\n");
			
			Socket connectionSocket = welcomeSocket.accept();
			
			BufferedReader inFromClient = new BufferedReader(new InputStreamReader(
								connectionSocket.getInputStream()));
			
			DataOutputStream outToClient = new DataOutputStream(
								connectionSocket.getOutputStream()); 
			
			clientIn = inFromClient.readLine();
			
			System.out.println("[SERVER] Received: "+clientIn+"\n");
			
			JSONObject o = new JSONObject(clientIn);
			if(o.getInt(Constants.met)==Constants.JSON_KEY_ONESHOT_REQ) {
				
				byte[] bytes = {2,3,4};
				sExchange_tPacket data = 
					new sExchange_tPacket(new sId_tPacket((byte)0,(byte)0),
						bytes);
				//outToClient.writeBytes(data.convertObjectToJSON()+"\n");
				
				outToClient.writeBytes("pippo"+"\n");
				
			} else if (o.getInt(Constants.met)==Constants.JSON_KEY_PERIODIC_REQ) {
				
				byte[] bytes1 = {2,3,4};
				sExchange_tPacket data1 = 
					new sExchange_tPacket(new sId_tPacket((byte)0,(byte)0),
						bytes1);
				
				byte[] bytes2 = {23,33,43};
				sExchange_tPacket data2 = 
					new sExchange_tPacket(new sId_tPacket((byte)0,(byte)0),
						bytes2);
				
				byte[] bytes3 = {21,1,2};
				sExchange_tPacket data3 = 
					new sExchange_tPacket(new sId_tPacket((byte)0,(byte)0),
						bytes3);
				
				Vector<sExchange_tPacket> v1 = new Vector<sExchange_tPacket>();
				v1.add(data1);
				v1.add(data2);
				sExchange_tListPacket pp1 = new sExchange_tListPacket(v1);
				
				Vector<sExchange_tPacket> v2 = new Vector<sExchange_tPacket>();
				v2.add(data3);
				v2.add(data1);
				sExchange_tListPacket pp2 = new sExchange_tListPacket(v2);
				
				
				JSONArray a1 = new JSONArray(pp1.convertObjectToJSON());
				JSONArray a2 = new JSONArray(pp2.convertObjectToJSON());
				
				
				JSONObject o11 = new JSONObject();
				JSONObject o12 = new JSONObject();
				o11.put("object", a1);
				o11.put("return", "sdepOK");
				
				o12.put("object", a2);
				o12.put("return", "sdepOK");

				JSONArray arr1 = new JSONArray();
				arr1.put(o11);
				arr1.put(o12);
				
				
				
				JSONArray arr2 = new JSONArray();
				arr2.put((byte)0);
				arr2.put((byte)1);
				
				JSONObject ogg = new JSONObject();
				ogg.put("data", arr1);
				ogg.put("pAddrList", arr2);
				
				outToClient.writeBytes(ogg+"\n");
				
				
				
			}
			
			/*capitalizedSentence = clientIn.toUpperCase()+ "\n"; 
			outToClient.writeBytes(capitalizedSentence);*/
		
		}
	}

}
