package packets;

import org.json.JSONException;
import org.json.JSONObject;

import exception.sError_tException;

public class sConfig_tPacket implements MyJSON_Interface {
	
	private sId_tPacket sensorId;
	private byte sensorMode;
	private byte sensorConfig;
	
	
	public sConfig_tPacket() {
		super();
	}


	public sConfig_tPacket(sId_tPacket sensorId, byte sensorMode, byte sensorConfig) {
		super();
		this.sensorId = sensorId;
		this.sensorMode = sensorMode;
		this.sensorConfig = sensorConfig;
	}


	public sId_tPacket getSensorId() {
		return sensorId;
	}


	public void setSensorId(sId_tPacket sensorId) {
		this.sensorId = sensorId;
	}


	public byte getSensorMode() {
		return sensorMode;
	}


	public void setSensorMode(byte sensorMode) {
		this.sensorMode = sensorMode;
	}


	public byte getSensorConfig() {
		return sensorConfig;
	}


	public void setSensorConfig(byte sensorConfig) {
		this.sensorConfig = sensorConfig;
	}


	@Override
	public String convertObjectToJSON() throws JSONException {

		JSONObject obj = new JSONObject(); 
		
		sId_tPacket id = sensorId;
		JSONObject idJson = new JSONObject(id.convertObjectToJSON());
	
		obj.put(Constants.SENS_ID, idJson); 
		obj.put(Constants.CON_MODE, this.sensorMode); 
		obj.put(Constants.CON_PER, this.sensorConfig);

		return obj.toString(); 
		
	}


	@Override
	public void convertJSONToObject(String o) throws JSONException, sError_tException {
		
		sId_tPacket id = new sId_tPacket();		
					
		JSONObject obj = new JSONObject(o);

		//case returnValue==sdepOK
		//if (objj.get(Constants.ret).toString().equals(Constants.OK)) {
			try {
				
				//JSONObject obj = objj.getJSONObject(Constants.obj); 
				JSONObject json_id = obj.getJSONObject(Constants.SENS_ID);
				
				id.convertJSONToObject(json_id.toString());
				
				this.sensorId = id; 
				
				this.sensorMode = (byte)obj.getInt(Constants.CON_MODE);
				this.sensorConfig = (byte)obj.getInt(Constants.CON_PER);
			} catch (JSONException e) {
				e.printStackTrace();
				sError_tListPacket err = new sError_tListPacket();
				err.convertJSONToObject(o);	
				throw new sError_tException(Constants.errStr,err);
			}
		//} else {
			//throw new sError_tException(objj.get(Constants.ret).toString());
		//}

	}


	@Override
	public String toString() {
		return "sConfig_tPacket [sensorId=" + sensorId + ", sensorMode="
				+ sensorMode + ", sensorConfig=" + sensorConfig + "]";
	}
	
	
	
	
	

}
