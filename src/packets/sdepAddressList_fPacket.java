package packets;

import org.json.JSONException;
import org.json.JSONObject;

import exception.sError_tException;

public class sdepAddressList_fPacket implements MyJSON_Interface {
	
	private byte pAddr;
	private int pSerial;
	
	
	public sdepAddressList_fPacket(byte pAddr, int pSerial) {
		super();
		this.pAddr = pAddr;
		this.pSerial = pSerial;
	}
	
	public sdepAddressList_fPacket() {
	}
	
	
	public byte getpAddr() {
		return pAddr;
	}
	
	public void setpAddr(byte pAddr) {
		this.pAddr = pAddr;
	}
	
	public int getpSerial() {
		return pSerial;
	}
	
	public void setpSerial(int pSerial) {
		this.pSerial = pSerial;
	}

	@Override
	public String convertObjectToJSON() throws JSONException {

		JSONObject o = new JSONObject(); 
		
		o.put(Constants.paddr, this.pAddr); 
		o.put(Constants.PSER, this.pSerial); 	
		
		return o.toString(); 
		
	}

	@Override
	public void convertJSONToObject(String s) throws JSONException, sError_tException {

		JSONObject o = new JSONObject(s);

		//case returnValue==sdepOK
		//if (obj.get(Constants.ret).toString().equals(Constants.OK)) {
			try {
				//JSONObject o = obj.getJSONObject(Constants.obj);
				this.pAddr = (byte)o.getInt(Constants.paddr);
				this.pSerial = o.getInt(Constants.PSER);
			} catch (JSONException e) {
				e.printStackTrace();
				sError_tListPacket err = new sError_tListPacket();
				err.convertJSONToObject(s);	
				throw new sError_tException(Constants.errStr,err);			
			}
		//} else {
			//throw new sError_tException(obj.get(Constants.ret).toString());
		//}
		
	}

	@Override
	public String toString() {
		return "sdepAddressList_fPacket [pAddr=" + pAddr + ", pSerial="
				+ pSerial + "]";
	}	
	
	

}
