package packets;

import java.util.Vector;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import exception.sError_tException;

public class PeriodicConfigList implements MyJSON_Interface {
	
	Vector<PeriodicConfig> perConfList;
	
	
	public PeriodicConfigList() {
		super();
	}
	
	
	public PeriodicConfigList(Vector<PeriodicConfig> perConfList) {
		super();
		this.perConfList = perConfList;
	}
	
	
	public Vector<PeriodicConfig> getPerConfList() {
		return perConfList;
	}


	public void setPerConfList(Vector<PeriodicConfig> perConfList) {
		this.perConfList = perConfList;
	}


	@Override
	public String convertObjectToJSON() throws JSONException {
		
		JSONArray arr = new JSONArray();
		
		for(int i=0; i<perConfList.size(); i++) {
			JSONObject o = new JSONObject(perConfList.get(i).convertObjectToJSON());
			arr.put(o);
		}
		
		return arr.toString();

		
	}
	
	
	@Override
	public void convertJSONToObject(String o) throws JSONException,
			sError_tException {
		// TODO Auto-generated method stub
		
	}


	@Override
	public String toString() {
		return "PeriodicConfigList [perConfList=" + perConfList + "]";
	}
	
	
	
	

}
