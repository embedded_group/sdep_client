package packets;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import exception.sError_tException;

public class PeriodicConfig implements MyJSON_Interface {
	
	byte pAddr;
	sConfig_tListPacket confList;
	int scNum;
	
	
	public PeriodicConfig() {
		super();
	}


	public PeriodicConfig(byte pAddr, sConfig_tListPacket confList, int scNum) {
		super();
		this.pAddr = pAddr;
		this.confList = confList;
		this.scNum = scNum;
	}


	public byte getpAddr() {
		return pAddr;
	}


	public void setpAddr(byte pAddr) {
		this.pAddr = pAddr;
	}


	public sConfig_tListPacket getConfList() {
		return confList;
	}


	public void setConfList(sConfig_tListPacket confList) {
		this.confList = confList;
	}


	public int getScNum() {
		return scNum;
	}


	public void setScNum(int scNum) {
		this.scNum = scNum;
	}


	@Override
	public String convertObjectToJSON() throws JSONException {

		JSONObject obj = new JSONObject(); 
		
		sConfig_tListPacket list = this.confList;
		JSONArray listJson = new JSONArray(list.convertObjectToJSON());
	
		obj.put(Constants.confList, listJson); 
		obj.put(Constants.paddr, this.pAddr); 
		obj.put(Constants.confNum, this.scNum);

		return obj.toString(); 
		
	}


	@Override
	public void convertJSONToObject(String o) throws JSONException,
			sError_tException {
		// TODO Auto-generated method stub
		
	}


	@Override
	public String toString() {
		return "PeriodicConfig [pAddr=" + pAddr + ", confList=" + confList
				+ ", scNum=" + scNum + "]";
	}
	
	
	

}
