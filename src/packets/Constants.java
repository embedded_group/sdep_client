package packets;


public class Constants {
	
	/*JSON OBJECTS KEYS*/
	public static final String met = "methodName";
	public static final String paddr = "pAddress";
	public static final String obj = "object";
	public static final String var = "variableField";
	public static final String NULL = "null";
	public static final String ret = "return";
	public static final String OK = "sdepOK";
	
	//checkAlive
	public static final int C_ALIVE = 0;
	
	//sendDisconnect
	public static final int S_DISCONNECT = 1;
	
	//sendCapabilityRequest
	public static final int S_CAPABILITY = 2;
	
	//sendConfigTo
	public static final int S_CONFIG = 3;
	
	//sendSamplesRequest
	public static final int S_SAMPLE_REQ = 4;
	
	//getAttachedList
	public static final int G_ATT_LIST = 5;	
	
	//reqOneShot
	public static final int JSON_KEY_ONESHOT_REQ = 6;

	//reqPeriodicElaboration
	public static final int JSON_KEY_PERIODIC_REQ = 7;
	
	
	
	/*sCapability_t KEYS*/
	public static final String CAP_QUAN = "sensorQuantity";
	public static final String CAP_LEN = "sensorCap";
	public static final String CAP_LIST = "sCapability_tList";
	
	
	/*sConfig_t KEYS*/
	public static final String SENS_ID = "sensorId";
	public static final String CON_MODE = "sensorMode";
	public static final String CON_PER = "sensorConfig";
	
	
	/*sId_t KEYS*/
	public static final String SENS_NUM = "sensorNum";
	public static final String SENS_TYPE = "sensorType";
	
	/*PeriodicConfigList KEYS*/
	public static final String confList = "configList";
	public static final String confNum = "scNum";
	public static final String data = "data";
	public static final String addrL = "pAddrList"; 
	
	
	
	
	/*sampleData KEYS */
	public static final String JSON_KEY_SAMPLE_INTERVAL= "samplingInt";
	public static final String JSON_KEY_SAMPLING_TIME= "timeSampling";
 	public static final String JSON_KEY_SAMPLE_MODE = "samplingMode";
	
	/********************************/ 
	
	
	
	/*sError_t KEYS*/
	public static final String ERR_CODE = "errorCode";
	
	
	/*sExchange_t KEYS*/
	public static final String SAM_PKT = "samplesPacket";
	public static final String TYPE = "intType";
	
	
	/*Socket PORT*/
	public static final int PORT = 6666;
	
	
	/*sdepAddressList_f KEYS*/
	public static final String PSER = "pSerial";
	
	/*Exception names*/
	public static final String errStr = "sError_tList";
	public static final String anoErr = "anotherError";
	public static final String IP = "192.168.2.100";
	
 

	public static final String JSON_KEY_SAMPLE_LEN = "sampleLen";
	public static final byte ACCELEROMETER_ID = 2; 
 
}
