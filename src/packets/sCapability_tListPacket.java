package packets;

import java.util.Vector;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import exception.sError_tException;

public class sCapability_tListPacket implements MyJSON_Interface {
	
	private Vector<sCapability_tPacket> caps;

	public sCapability_tListPacket(Vector<sCapability_tPacket> caps) {
		super();
		this.caps = caps;
	}

	public sCapability_tListPacket() {
		// TODO Auto-generated constructor stub
	}

	public Vector<sCapability_tPacket> getCaps() {
		return caps;
	}

	public void setCaps(Vector<sCapability_tPacket> caps) {
		this.caps = caps;
	}

	@Override
	public String convertObjectToJSON() throws JSONException {

		JSONArray arr = new JSONArray();
		
		for(int i=0; i<caps.size(); i++) {
			JSONObject o = new JSONObject(caps.get(i).convertObjectToJSON());
			arr.put(o);
		}
		
		return arr.toString();
		
	}

	@Override
	public void convertJSONToObject(String s) throws JSONException, sError_tException {	
		Vector<sCapability_tPacket> v = new Vector<sCapability_tPacket>();
		JSONObject obj1 = new JSONObject(s);
		
		//case returnValue==sdepOK
		if (obj1.get(Constants.ret).toString().equals(Constants.OK)) {
			try {
				JSONArray o = obj1.getJSONArray(Constants.obj);
				
				for(int i=0; i<o.length(); i++) {
					JSONObject obj = o.getJSONObject(i);
					sCapability_tPacket c = new sCapability_tPacket();
					c.convertJSONToObject(obj.toString());
					v.add(c);
				}
				
				this.caps = v;
			} catch (JSONException e) {
				e.printStackTrace();
				sError_tListPacket err = new sError_tListPacket();
				err.convertJSONToObject(s);	
				throw new sError_tException(Constants.errStr,err);
			}
		} else {
			throw new sError_tException(obj1.get(Constants.ret).toString());
		}

	}

	
	//DEBUG
	@Override
	public String toString() {
		return "sCapability_tListPacket [caps=" + caps + "]";
	}
	

}
