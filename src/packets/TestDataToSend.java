package packets;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.Vector;

import org.json.JSONException;

import exception.sError_tException;

public class TestDataToSend {
	
	
	public static void main(String args[]) throws UnknownHostException,
		JSONException, IOException, sError_tException {
		
		PacketBuilder pktBdr = new PacketBuilder(Constants.IP, Constants.PORT);
		
		//testOneShot(pktBdr);
		
		
		testPeriodic(pktBdr);

		
		
	}

	public static void testPeriodic(PacketBuilder pktBdr) throws JSONException,
			UnknownHostException, IOException, sError_tException {
		
		sId_tPacket id1 = new sId_tPacket((byte)0,(byte)0);
		sId_tPacket id2 = new sId_tPacket((byte)1,(byte)0);
		sId_tPacket id3 = new sId_tPacket((byte)2,(byte)0);
		sId_tPacket id4 = new sId_tPacket((byte)3,(byte)0);
		
		sConfig_tPacket c1 = new sConfig_tPacket(id1,(byte)0,(byte)0);
		sConfig_tPacket c2 = new sConfig_tPacket(id2,(byte)176,(byte)1);
		sConfig_tPacket c3 = new sConfig_tPacket(id3,(byte)2,(byte)0);
		sConfig_tPacket c4 = new sConfig_tPacket(id4,(byte)3,(byte)0);
		Vector<sConfig_tPacket> v = new Vector<sConfig_tPacket>();
		v.add(c1);
		v.add(c2);
		v.add(c3);
		v.add(c4);

		
		sConfig_tListPacket confList = new sConfig_tListPacket(v);
		
		sId_tPacket id15 = new sId_tPacket((byte)0,(byte)2);
		sId_tPacket id25 = new sId_tPacket((byte)1,(byte)3);
		sId_tPacket id35 = new sId_tPacket((byte)2,(byte)4);
		sId_tPacket id45 = new sId_tPacket((byte)3,(byte)5);
		
		sConfig_tPacket c15 = new sConfig_tPacket(id15,(byte)2,(byte)1);
		sConfig_tPacket c25 = new sConfig_tPacket(id25,(byte)176,(byte)1);
		sConfig_tPacket c35 = new sConfig_tPacket(id35,(byte)3,(byte)2);
		sConfig_tPacket c45 = new sConfig_tPacket(id45,(byte)4,(byte)3);
		Vector<sConfig_tPacket> v5 = new Vector<sConfig_tPacket>();
		v5.add(c15);
		v5.add(c25);
		v5.add(c35);
		v5.add(c45);

		sConfig_tListPacket confList5 = new sConfig_tListPacket(v5);
		
		PeriodicConfig p = new PeriodicConfig((byte)0, confList, 4);
		PeriodicConfig p2 = new PeriodicConfig((byte)1, confList5, 4);
		
		Vector<PeriodicConfig> ppp = new Vector<PeriodicConfig>();
		ppp.add(p);
		ppp.add(p2);		
		
		PeriodicConfigList pL = new PeriodicConfigList(ppp);

		Vector<PeriodicData> data = new Vector<PeriodicData>();
		data = pktBdr.reqPeriodicElaboration(pL, 3);
		System.out.println("Uscito fuori da reqPeriodicElaboration");
		System.out.println("Res from "+Constants.JSON_KEY_PERIODIC_REQ+
				": "+data.toString());
		
	}

	public static void testOneShot(PacketBuilder pktBdr) throws JSONException,
			UnknownHostException, IOException, sError_tException {
		
		sId_tPacket id1 = new sId_tPacket((byte)0,(byte)0);

		sExchange_tPacket data = new sExchange_tPacket();
		data = pktBdr.reqOneShotData((byte)0, id1);
		
		System.out.println("Res from "+Constants.JSON_KEY_ONESHOT_REQ+
				": "+data.toString());
	}

}
