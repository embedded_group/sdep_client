package packets;

public class PeriodicData {
	
	byte pAddr;
	sExchange_tListPacket data;
	
	
	public PeriodicData() {
		super();
	}


	public PeriodicData(byte pAddr, sExchange_tListPacket data) {
		super();
		this.pAddr = pAddr;
		this.data = data;
	}


	public byte getpAddr() {
		return pAddr;
	}


	public void setpAddr(byte pAddr) {
		this.pAddr = pAddr;
	}


	public sExchange_tListPacket getData() {
		return data;
	}


	public void setData(sExchange_tListPacket data) {
		this.data = data;
	}


	@Override
	public String toString() {
		return "PeriodicData [pAddr=" + pAddr + ", data=" + data + "]";
	}
	

	

}
