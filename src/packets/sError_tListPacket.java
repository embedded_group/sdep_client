package packets;

import java.util.Vector;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import exception.sError_tException;

public class sError_tListPacket implements MyJSON_Interface {
	
	private Vector<sError_tPacket> errorList;

	public sError_tListPacket(Vector<sError_tPacket> errorList) {
		super();
		this.errorList = errorList;
	}

	public sError_tListPacket() {
	}

	public Vector<sError_tPacket> getErrorList() {
		return errorList;
	}

	public void setErrorList(Vector<sError_tPacket> errorList) {
		this.errorList = errorList;
	}

	@Override
	public String convertObjectToJSON() throws JSONException {

		JSONArray arr = new JSONArray();
		
		for(int i=0; i<errorList.size(); i++) {
			JSONObject o = new JSONObject(errorList.get(i).convertObjectToJSON());
			arr.put(o);
		}
		
		return arr.toString();
		
	}

	@Override
	public void convertJSONToObject(String s) throws JSONException, sError_tException {
		
		Vector<sError_tPacket> v = new Vector<sError_tPacket>();
		
		JSONObject o1 = new JSONObject(s);
		
		//case returnValue==sdepOK
		//if (o1.get(Constants.ret).toString().equals(Constants.OK)) {
			try {
				
				JSONArray o = o1.getJSONArray(Constants.obj);
				
				for(int i=0; i<o.length(); i++) {
					JSONObject obj = o.getJSONObject(i);
					sError_tPacket c = new sError_tPacket();
					c.convertJSONToObject(obj.toString());
					v.add(c);
				}
				
				this.errorList = v;
			} catch (JSONException e) {
				e.printStackTrace();
				//sError_tListPacket err = new sError_tListPacket();
				//err.convertJSONToObject(s);	
				throw new sError_tException(Constants.anoErr);
			}
		//} else {
			//throw new sError_tException(o1.get(Constants.ret).toString());
		//}
	}

	@Override
	public String toString() {
		return "sError_tListPacket [errorList=" + errorList + "]";
	}

	
	
	
	

}
